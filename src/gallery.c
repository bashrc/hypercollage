/*
  hypercollage - Evolutionary collage maker
  Copyright (C) 2017, 2019 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "hypercollage.h"

/**
 * @brief checks if the given image is mono
 * @param img Image array
 * @param image_width Width of the image
 * @param image_height Height of the image
 * @return True on success
 */
int image_is_mono(unsigned char img[],
                  unsigned int  image_width, unsigned int image_height)
{
    for (unsigned int i = (image_width*image_height*3)-3; i >= 0; i-=3)
        if ((img[i] != img[i+1]) || (img[i] != img[i+2]))
            return 1;

    return 0;
}

/**
 * @brief Extract segments from an image
 * @param img Image instance
 * @returns Zero on success
 */
int extract_segments_from_image(image * img)
{
    img->segment_map =
        (unsigned char*)malloc(img->image_width*img->image_height*
                               sizeof(unsigned char));
    if (!img->segment_map) {
        printf("Unable to allocate memory for segmentation" \
               "map of dimensions %dx%d\n",
               img->image_width, img->image_height);
        return 1;
    }
    create_segments_map(img->image_data, img->image_width, img->image_height,
                        img->segment_map);
    return 0;
}

/**
 * @brief Creates a gallery from a directory containing source png images
 * @param directory The directory to search
 * @param image_gallery Gallery containing source images
 * @returns Zero on success
 */
int gallery_create(char * directory, gallery * image_gallery)
{
    DIR *dir;
    struct dirent *ent;
    unsigned int image_bitsperpixel=0;
    char filename[1024];
    int no_of_images;

    image_gallery->no_of_images = 0;

    if ((dir = opendir(directory)) != NULL) {
        /* for each image in the directory */
        while ((ent = readdir (dir)) != NULL) {
            if (strstr(ent->d_name,".png") != NULL) {
                sprintf(filename,"%s/%s",directory, ent->d_name);
                no_of_images = image_gallery->no_of_images;
                image_gallery->pic[no_of_images].image_data =
                    read_png_file(filename,
                                  &image_gallery->pic[no_of_images].image_width,
                                  &image_gallery->pic[no_of_images].image_height,
                                  &image_bitsperpixel);
                if (image_gallery->pic[no_of_images].image_data != NULL) {
                    if (image_bitsperpixel == 24) {
                        if (image_is_mono(image_gallery->pic[no_of_images].image_data,
                                          image_gallery->pic[no_of_images].image_width,
                                          image_gallery->pic[no_of_images].image_height) != 0) {
                            printf("Segmenting %s\n", ent->d_name);
                            if (extract_segments_from_image(&image_gallery->pic[no_of_images]) == 0) {
                                image_gallery->no_of_images++;
                                if (image_gallery->no_of_images >= MAX_IMAGES) break;
                            }
                        }
                        else {
                            free(image_gallery->pic[image_gallery->no_of_images].image_data);
                        }
                    }
                    else {
                        free(image_gallery->pic[image_gallery->no_of_images].image_data);
                    }
                }
            }
        }
        closedir (dir);
    }
    else {
        /* could not open directory */
        perror ("");
        return EXIT_FAILURE;
    }

    return 0;
}

/**
 * @brief Deallocate memory for gallery instance
 * @param image_gallery Gallery containing source images
 */
void gallery_free(gallery * image_gallery)
{
    for (unsigned int i = 0; i < image_gallery->no_of_images; i++) {
        free(image_gallery->pic[i].image_data);
        free(image_gallery->pic[i].segment_map);
    }
}
