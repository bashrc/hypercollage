/*
  hypercollage - Evolutionary collage maker
  Copyright (C) 2017,2019 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "hypercollage.h"

/**
 * @brief Mutates the give gene
 * @param g Gene to be mutated
 * @param image_gallery Gallery of source images
 * @param probability Mutation probability
 */
void mutate_gene(gene * g, gallery * image_gallery, float probability)
{
    if (MUTATION(probability))
        g->image_index =
            (unsigned char)(rand() % (int)image_gallery->no_of_images);

    if (MUTATION(probability))
        g->segment_index = (unsigned char)(rand() % 32);

    if (MUTATION(probability))
        g->colour_operation = (unsigned char)(rand() % COLOUR_OPERATIONS);

    if (MUTATION(probability))
        g->opacity =
            (unsigned char)(((int)g->opacity + (int)(rand() % 10) - 5) % 255);

    if (MUTATION(probability))
        g->move_operation = (unsigned char)(rand() % MOVE_OPERATIONS);

    if (MUTATION(probability))
        g->translate_x += (int)(rand() % 10) - 5;

    if (MUTATION(probability))
        g->translate_y += (int)(rand() % 10) - 5;

    if (MUTATION(probability))
        g->rotation += (int)(rand() % 10) - 5;

    if (MUTATION(probability)) {
        g->maxscale += (int)(rand() % 10) - 5;
        if (g->maxscale < 1) g->maxscale = 1;
    }

    if (MUTATION(probability)) {
        g->scale += (int)(rand() % 10) - 5;
        if (g->scale < 1) g->scale = 1;
        if (g->scale > g->maxscale) g->scale = g->maxscale;
    }

    if (MUTATION(probability))
        g->red += ((int)(rand() % 10) - 5) % 256;

    if (MUTATION(probability))
        g->green += ((int)(rand() % 10) - 5) % 256;

    if (MUTATION(probability))
        g->blue += ((int)(rand() % 10) - 5) % 256;

    if (MUTATION(probability))
        g->dotsize = 1 + (int)(rand() % 2);

    if (MUTATION(probability))
        g->merge = (int)(rand() % 9);
}

/**
 * @brief Mutate the given sample
 * @param s Sample to be mutated
 * @param image_gallery Gallery of source images
 * @param probability Mutation probability
 */
void mutate_sample(sample * s, gallery * image_gallery, float probability)
{
    unsigned int i, j;
    gene temp;

    /* mutate the length */
    if (MUTATION(probability)) {
        if ((s->length > 1) && (s->length < GENES_PER_SAMPLE-1)) {
            if (rand() % 1000 > 500)
                s->length++;
            else
                s->length--;
        }
    }

    for (i = 0; i < GENES_PER_SAMPLE; i++)
        mutate_gene(&s->genome[i], image_gallery, probability);

    if (MUTATION(probability)) {
        i = (int)(rand() % GENES_PER_SAMPLE);
        j = (int)(rand() % GENES_PER_SAMPLE);
        if (i != j) {
            memcpy((void*)&temp, (void*)&s->genome[i], sizeof(gene));
            memcpy((void*)&s->genome[i], (void*)&s->genome[j], sizeof(gene));
            memcpy((void*)&s->genome[j], &temp, sizeof(gene));
        }
    }
}

/**
 * @brief Breed two samples to produce a child
 * @param parent1 The first sample
 * @param parent2 The second sample
 * @param child The resulting child sample
 * @param crossover_probability The probability of crossover
 */
void crossover_sample(sample * parent1, sample * parent2,
                      sample * child,
                      float crossover_probability)
{
    unsigned int i, crossover_point;

    if (MUTATION(crossover_probability)) {
        /* make a clone of one parent of the other */
        if (rand() % 1000 > 500) {
            child->length = parent1->length;
            memcpy((void*)&child->genome[0],
                   (void*)&parent1->genome[0],sizeof(gene)*GENES_PER_SAMPLE);
        }
        else {
            child->length = parent2->length;
            memcpy((void*)&child->genome[0],
                   (void*)&parent2->genome[0],sizeof(gene)*GENES_PER_SAMPLE);
        }
        return;
    }

    if (rand() % 1000 > 500)
        child->length = parent1->length;
    else
        child->length = parent2->length;

    /* crossover the parents to produce the child */
    crossover_point = (unsigned int)(rand() % GENES_PER_SAMPLE);
    for (i = 0; i < GENES_PER_SAMPLE; i++)
        if (i < crossover_point)
            memcpy((void*)&child->genome[i],
                   (void*)&parent1->genome[i],sizeof(gene));
        else
            memcpy((void*)&child->genome[i],
                   (void*)&parent2->genome[i],sizeof(gene));
}

/**
 * @brief Creates a gene
 * @param g The gene to be created
 * @param image_gallery Gallery of source images
 */
void create_gene(gene * g, gallery * image_gallery)
{
    g->image_index =
        (unsigned char)(rand() % (int)image_gallery->no_of_images);
    g->segment_index = (unsigned char)(rand() % 32);
    g->colour_operation = (unsigned char)(rand() % COLOUR_OPERATIONS);
    g->opacity = (unsigned char)(rand() % 256);
    g->move_operation = (unsigned char)(rand() % MOVE_OPERATIONS);
    g->translate_x = (int)(rand() % 200) - 100;
    g->translate_y = (int)(rand() % 200) - 100;
    g->rotation = (int)(rand() % 360) - 180;
    g->maxscale = (int)(1 + (rand() % 500));
    g->scale = 10 + (int)(rand() % g->maxscale);
    g->red = (int)(rand() % 256);
    g->green = (int)(rand() % 256);
    g->blue = (int)(rand() % 256);
    g->dotsize = 1 + (int)(rand() % 2);
    g->merge = (int)(rand() % 9);
}

/**
 * @brief Creates a sample
 * @param s The sample to be created
 * @param image_gallery Gallery of source images
 */
void create_sample(sample * s, gallery * image_gallery)
{
    unsigned int i;

    s->length = 2 + (int)(rand() % (GENES_PER_SAMPLE-2));
    for (i = 0; i < GENES_PER_SAMPLE; i++)
        create_gene(&s->genome[i], image_gallery);
}


/**
 * @brief Creates a population of samples
 * @param p The population to be created
 * @param image_gallery Gallery of source images
 */
void create_population(population * p, gallery * image_gallery)
{
    unsigned int i;

    for (i = 0; i < POPULATION_SIZE; i++)
        create_sample(&p->individual[i], image_gallery);
}

/**
 * @brief Create the next generation of samples
 * @param p The population instance
 * @param image_gallery Gallery of source images
 * @param crossover_probability Probability of crossover
 * @param mutation_probability Probability of mutation
 * @param winner_index Index of the highest scoring sample
 */
void generation_cycle(population * p, gallery * image_gallery,
                      float crossover_probability,
                      float mutation_probability,
                      unsigned int winner_index)
{
    unsigned int i;
    sample child;

    for (i = 0; i < POPULATION_SIZE; i++) {
        if (i == winner_index)
            continue;
        crossover_sample(&p->individual[i],
                         &p->individual[winner_index],
                         &child, crossover_probability);
        mutate_sample(&child, image_gallery, mutation_probability);
        memcpy((void*)&p->individual[i], &child, sizeof(sample));
    }
}

/**
 * @brief Draw a sample
 * @param p The population instance
 * @param image_gallery Gallery of source images
 * @param index Index of the sample to draw
 * @param filename Filename to save the sample as
 * @param output_image_width Width of the sample image to save
 * @param output_image_height Height of the sample image to save
 */
void draw_population_sample(population * p, gallery * image_gallery,
                            int index,
                            char * filename,
                            unsigned int output_image_width,
                            unsigned int output_image_height)
{
    unsigned char * output_image =
        (unsigned char *)malloc(output_image_width*output_image_height*3*
                                sizeof(unsigned char));
    if (!output_image) return;
    sample_draw(&p->individual[index], image_gallery,
                output_image,
                output_image_width,
                output_image_height);

    write_png_file(filename,
                   output_image_width, output_image_height,
                   24, output_image);
    free(output_image);
}

/**
 * @brief Save the population
 * @param p The population instance
 * @param filename The filename to save as
 */
void save_population(population * p, char * filename)
{
    FILE * fp = fopen(filename, "wb");
    if (!fp) return;
    fwrite((void*)p, sizeof(population), 1, fp);
    fclose(fp);
}

/**
 * @brief Load the population
 * @param p The population instance
 * @param filename The filename to load from
 */
void load_population(population * p, char * filename)
{
    FILE * fp = fopen(filename, "rb");
    if (!fp) return;
    fread((void*)p, sizeof(population), 1, fp);
    fclose(fp);
}
