/*
  hypercollage - Evolutionary collage maker
  Copyright (C) 2017,2019 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "hypercollage.h"

/**
 * @brief Returns the centre position of the given segment
 * @param img Image array
 * @param image_width Width of the image
 * @param image_height Height of the image
 * @param segment_index Index of the segment
 * @param segment_map[] Array containing segments
 * @param cx Returned centre x coordinate
 * @param cy Returned centre y coordinate
 */
void get_segment_centre(unsigned char img[],
                        unsigned int  image_width, unsigned int image_height,
                        unsigned char segment_index,
                        unsigned char segment_map[],
                        unsigned int * cx, unsigned int * cy)
{
    unsigned int x, y, hits=0;

    *cx = 0;
    *cy = 0;
    for (y = 0; y < image_height; y++) {
        for (x = 0; x < image_width; x++) {
            if (segment_map[y*image_width + x] == segment_index) {
                *cx = *cx + x;
                *cy = *cy + y;
                hits++;
            }
        }
    }
    if (hits > 0) {
        *cx = *cx / hits;
        *cy = *cy / hits;
    }
}

/**
 * @brief Create a segments map from the given image
 * @param img Image array
 * @param image_width Width of the image
 * @param image_height Height of the image
 * @param segment_map Array containing segments
 */
void create_segments_map(unsigned char img[],
                         unsigned int  image_width, unsigned int image_height,
                         unsigned char segment_map[])
{
    unsigned int i, a, w, x, y, z, n, d, e;
    unsigned int average[3], av_edge, av_variance, v;
    unsigned int pixels = image_width*image_height;

    /* calculate average intensity for each color channel */
    for (d = 0; d < 3; d++) {
        average[d] = img[d];

        for (i = 3; i < pixels*3; i+=3)
            average[d] += img[i+d];

        average[d] /= pixels;
    }

    /* calculate average variance */
    av_variance = 0;
    av_edge = 0;
    for (i = 9; i < pixels*3; i+=3) {
        av_variance += abs((int)img[i] - (int)img[i+1]) + \
            abs((int)img[i] - (int)img[i+2]) + \
            abs((int)img[i+1] - (int)img[i+2]);
        av_edge += abs((int)img[i-9] - (int)img[i]);
    }
    av_variance /= (pixels-3);
    av_edge /= (pixels-3);

    /* update the segment map
       Check if each pixel is above or below the average for each color channel
       and also above or below average variance
       This then buckets it into 16 possible segments
     */
    n = 3;
    segment_map[0] = 0;
    for (i = 9; i < pixels*3; i+=3, n++) {
        v = abs((int)img[i] - (int)img[i+1]) + \
            abs((int)img[i] - (int)img[i+2]) + \
            abs((int)img[i+1] - (int)img[i+2]);
        e = abs((int)img[i-9] - (int)img[i]);
        w = a = x = y = z = 0;
        if (e > av_edge) a = 1;
        if (v > av_variance) w = 1;
        if (img[i] > average[0]) x = 1;
        if (img[i+1] > average[1]) y = 1;
        if (img[i+2] > average[2]) z = 1;
        segment_map[n] = (a*16) + (w*8) + (x*4) + (y*2) + z;
    }
}
