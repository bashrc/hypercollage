/*
  hypercollage - Evolutionary collage maker
  Copyright (C) 2017,2019 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "hypercollage.h"

/**
 * @brief Transform a point relative to the given centre
 * @param centre_x x coordinate to rotate/scale from
 * @param centre_y y coordinate to rotate/scale from
 * @param x returned x coordinate to be transformed
 * @param y returned y coordinate to be transformed
 * @param scale Scaling percentage
 * @param rotation Rotation degrees
 * @param mirror Whether to mirror on the x axis
 * @param flip Whether to flip on the y axis
 */
void transform_point(int centre_x, int centre_y,
                     int * x, int * y,
                     int scale, int rotation,
                     int mirror, int flip)
{
    int dx = *x - centre_x;
    int dy = *y - centre_y;

    dx = dx*scale/100.0f;
    dy = dy*scale/100.0f;

    /* rotation*2*3.1415927f/360.0f; */
    float angle = rotation*0.0174532927778f;
    if (flip == 0)
        *y = centre_y - (int)(dx*cos(angle) - dy*sin(angle));
    else
        *y = centre_y + (int)(dx*cos(angle) - dy*sin(angle));

    if (mirror == 0)
        *x = centre_x - (int)(dx*sin(angle) - dy*cos(angle));
    else
        *x = centre_x + (int)(dx*sin(angle) - dy*cos(angle));
}

/**
 * @brief Draws a single point in the output image
 * @param output_image Image to draw to
 * @param output_image_width Width of the output image
 * @param output_image_height Height of the output image
 * @param x X coordinate of the point
 * @param y Y coordinate of the point
 * @param radius Draw within this radius
 * @param r Red
 * @param g Green
 * @param b Blue
 * @param opacity Opacity in the range 0-255
 * @param merge The type of merge function
 */
static void gene_draw_dot(unsigned char output_image[],
                          unsigned int output_image_width,
                          unsigned int output_image_height,
                          int x, int y, int radius,
                          unsigned char r,
                          unsigned char g,
                          unsigned char b,
                          unsigned char opacity,
                          unsigned char merge)
{
    int xx,yy,n,dx,dy;
    int radius_squared = radius*radius;
    int inverse_opacity = 255 - (int)opacity;

    for (yy = y - radius; yy <= y + radius; yy++) {
        if ((yy < 0) || (yy >= (int)output_image_height))
            continue;
        dy = yy - y;
        n = (yy*output_image_width + x - radius)*3;
        for (xx = x - radius; xx <= x + radius; xx++, n += 3) {
            if ((xx < 0) || (xx >= (int)output_image_width))
                continue;
            dx = xx - x;

            if (dx*dx + dy*dy > radius_squared)
                continue;

            if (opacity == 0) {
                output_image[n] = b;
                output_image[n+1] = g;
                output_image[n+2] = r;
            }
            else {
                switch(merge) {
                case 0: {
                    PIXEL_CALC(output_image, n, opacity, inverse_opacity, b, g, r);
                    break;
                }
                case 1: {
                    PIXEL_CALC(output_image, n, opacity, inverse_opacity, r, g, b);
                    break;
                }
                case 2: {
                    PIXEL_CALC(output_image, n, opacity, inverse_opacity, r, b, g);
                    break;
                }
                case 3: {
                    PIXEL_CALC(output_image, n, opacity, inverse_opacity, r, r, r);
                    break;
                }
                case 4: {
                    PIXEL_CALC(output_image, n, opacity, inverse_opacity, g, g, g);
                    break;
                }
                case 5: {
                    PIXEL_CALC(output_image, n, opacity, inverse_opacity, b, b, b);
                    break;
                }
                case 6: {
                    PIXEL_CALC(output_image, n, opacity, inverse_opacity, r, g, r);
                    break;
                }
                case 7: {
                    PIXEL_CALC(output_image, n, opacity, inverse_opacity, g, r, g);
                    break;
                }
                case 8: {
                    PIXEL_CALC(output_image, n, opacity, inverse_opacity, b, g, b);
                    break;
                }
                }
            }
        }
    }
}

/**
 * @brief Draws a point to the output image using the give gene
 * @param g Gene instance
 * @param output_image Output image to draw to
 * @param output_image_width Width of the output image
 * @param output_image_height Height of the output image
 * @param x X coordinate of the point
 * @param y Y coordinate of the point
 * @param col Array of three bytes containing rgb values
 */
static void gene_draw_pixel(gene * g,
                            unsigned char output_image[],
                            unsigned int output_image_width,
                            unsigned int output_image_height,
                            int x, int y,
                            unsigned char col[])
{
    unsigned int output_image_n;
    unsigned char red2=0,green2=0,blue2=0;
    unsigned char opacity = g->opacity;

    switch(g->colour_operation) {
    case COLOUR_SAME: {
        red2=col[0];
        green2=col[1];
        blue2=col[2];
        break;
    }
    case COLOUR_INVERSE: {
        red2 = 255-col[0];
        green2 = 255-col[1];
        blue2 = 255-col[2];
        break;
    }
    case COLOUR_MONO: {
        red2 = green2 = blue2 = (unsigned char)(((int)col[0]+(int)col[1]+(int)col[2])/3);
        break;
    }
    case COLOUR_PRESET: {
        red2 = g->red;
        green2 = g->green;
        blue2 = g->blue;
        break;
    }
    }

    output_image_n = (y*output_image_width + x)*3;
    if (output_image[output_image_n] == 0) {
        /* if background is blank */
        opacity = 0;
    }
    gene_draw_dot(output_image,
                  output_image_width,
                  output_image_height,
                  x, y, g->dotsize,
                  red2, green2, blue2, opacity, g->merge);
}

/**
 * @brief Draws the given gene
 * @param g Gene instance
 * @param image_gallery The gallery of source images
 * @param output_image The image to draw to
 * @param output_image_width Width of the output image
 * @param output_image_height Height of the output image
 */
static void gene_draw(gene * g, gallery * image_gallery,
                      unsigned char output_image[],
                      unsigned int output_image_width,
                      unsigned int output_image_height)
{
    image * img;
    int x, y, xx, yy, gallery_x, gallery_y, gallery_n;
    unsigned int cx = 0, cy = 0;
    int mirror = 0, flip = 0;
    int prev_gallery_n = 999;

    if (g->move_operation == MOVE_FLIP) flip=1;
    if (g->move_operation == MOVE_MIRROR) mirror=1;

    if ((int)g->image_index >= (int)image_gallery->no_of_images) {
        printf("Image index %d out of range\n", (int)g->image_index);
        g->image_index %= (int)image_gallery->no_of_images;
    }

    img = &image_gallery->pic[g->image_index];

    get_segment_centre(img->image_data,
                       img->image_width, img->image_height,
                       g->segment_index,
                       img->segment_map,
                       &cx, &cy);

    for (y = output_image_height*2-1; y >= 0; y--) {
        gallery_y = y * img->image_height / (output_image_height*2);

        for (x = output_image_width*2-1; x >= 0; x--) {
            gallery_x = x * img->image_width / (output_image_width*2);
            gallery_n = gallery_y*img->image_width + gallery_x;

            if (prev_gallery_n == gallery_n)
                continue;

            if (img->segment_map[gallery_n] == g->segment_index) {
                xx = gallery_x;
                yy = gallery_y;
                transform_point(cx, cy, &xx, &yy,
                                (int)g->scale, (int)g->rotation,
                                mirror, flip);

                /* translate the point */
                xx = abs(xx + (g->translate_x*(int)output_image_width/100)) % (int)output_image_width;
                yy = abs(yy + (g->translate_y*(int)output_image_height/100)) % (int)output_image_height;

                /* draw the pixel at the transformed position */
                gene_draw_pixel(g, output_image,
                                output_image_width, output_image_height,
                                xx, yy, &img->image_data[gallery_n*3]);
            }

            prev_gallery_n = gallery_n;
        }
    }
}

/**
 * @brief Draws a given sample image
 * @param s Sample image instance
 * @param image_gallery The gallery of source images
 * @param output_image The image to draw to
 * @param output_image_width Width of the output image
 * @param output_image_height Height of the output image
 */
void sample_draw(sample * s, gallery * image_gallery,
                 unsigned char output_image[],
                 unsigned int output_image_width,
                 unsigned int output_image_height)
{
    unsigned int i;

    memset((void*)output_image, '\0',
           output_image_width*output_image_height*3*
           sizeof(unsigned char));

    for (i = 0; i < s->length; i++) {
        gene_draw(&s->genome[i], image_gallery,
                  output_image,
                  output_image_width,
                  output_image_height);
    }
}
