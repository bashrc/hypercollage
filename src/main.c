/*
  hypercollage - Evolutionary collage maker
  Copyright (C) 2017,2019 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "hypercollage.h"

void show_help()
{
    printf("\nHyperCollage: Evolutionary art builder\n\n");
    printf(" -i --images [directory]     Directory containing input PNG images\n");
    printf(" -s --size [pixels]          Set resolution of images produced\n");
    printf(" -r --reset                  Reset to a new random set of output images\n");
    printf(" -w --winner [image number]  Specify the winning image number to breed from\n\n");
}

int main(int argc, char* argv[])
{
    int i, winner = -1;
    char * inputimages = NULL;
    char output_filename[255];
    char population_filename[255];
    char cmdstr[300];
    gallery image_gallery;
    population p;
    float crossover_probability=0.1f;
    float mutation_probability=0.1f;
    int image_size=1024;

    if (argc <= 1) {
        show_help();
        return 0;
    }

    srand(time(NULL));

    sprintf((char*)output_filename,"%s","result.png");
    sprintf((char*)population_filename,"%s",".population.dat");

    for (i = 1; i < argc; i+=2) {
        if ((strcmp(argv[i],"-i")==0) ||
            (strcmp(argv[i],"--images")==0) ||
            (strcmp(argv[i],"--inputimages")==0)) {
            inputimages = argv[i+1];
        }
        if ((strcmp(argv[i],"-p")==0) ||
            (strcmp(argv[i],"--population")==0)) {
            sprintf(population_filename, "%s", argv[i+1]);
        }
        if ((strcmp(argv[i],"-r")==0) ||
            (strcmp(argv[i],"--reset")==0)) {
            sprintf(cmdstr,"rm %s",population_filename);
            system(cmdstr);
            i--;
        }
        if ((strcmp(argv[i],"-w")==0) ||
            (strcmp(argv[i],"--win")==0) ||
            (strcmp(argv[i],"--wins")==0) ||
            (strcmp(argv[i],"--winner")==0)) {
            winner = atoi(argv[i+1]);
        }
        if ((strcmp(argv[i],"-s")==0) ||
            (strcmp(argv[i],"--size")==0)) {
            image_size = atoi(argv[i+1]);
        }
    }

    if (inputimages == NULL) {
        printf("Provide an input images directory with the -i option\n");
        return 1;
    }

    gallery_create(inputimages, &image_gallery);
    printf("%d source images loaded\n", image_gallery.no_of_images);

    create_population(&p, &image_gallery);
    load_population(&p, population_filename);

    if (winner > -1) {
        generation_cycle(&p, &image_gallery,
                         crossover_probability,
                         mutation_probability,
                         (unsigned int)winner);
    }

    for (i = 0; i < POPULATION_SIZE; i++) {
        if (i != winner) {
            sprintf(output_filename,"sample-%d.png", i);
            draw_population_sample(&p, &image_gallery,
                                   (unsigned int)i, output_filename,
                                   (unsigned int)image_size,
                                   (unsigned int)image_size);
        }
    }
    save_population(&p, population_filename);

    printf("Ended Successfully\n");
    return 0;
}
