/*
  hypercollage - Evolutionary collage maker
  Copyright (C) 2017,2019 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HYPERCOLLAGE_H
#define HYPERCOLLAGE_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <dirent.h>
#include <math.h>
#include <time.h>
#include "lodepng.h"

#define MAX_IMAGES       128
#define GENES_PER_SAMPLE 480
#define POPULATION_SIZE  (3*3)

enum {
    COLOUR_SAME = 0,
    COLOUR_INVERSE,
    COLOUR_MONO,
    COLOUR_PRESET,
    COLOUR_OPERATIONS
};

enum {
    MOVE_SAME = 0,
    MOVE_MIRROR,
    MOVE_FLIP,
    MOVE_OPERATIONS
};

typedef struct _image {
    unsigned char * image_data;
    unsigned int image_width;
    unsigned int image_height;
    unsigned char * segment_map;
} image;

typedef struct _gallery {
    image pic[MAX_IMAGES];
    unsigned int no_of_images;
} gallery;

typedef struct _gene {
    unsigned char image_index;
    unsigned char segment_index;
    unsigned char colour_operation;
    unsigned char opacity;
    unsigned char move_operation;
    unsigned char red;
    unsigned char green;
    unsigned char blue;

    unsigned char dotsize;
    unsigned char merge;
    unsigned char dummy3;
    unsigned char dummy4;
    unsigned char dummy5;
    unsigned char dummy6;
    unsigned char dummy7;
    int translate_x;
    int translate_y;
    int rotation;
    int maxscale;
    int scale;
} gene;

typedef struct _sample {
    unsigned int length;
    gene genome[GENES_PER_SAMPLE];
} sample;

typedef struct _population {
    sample individual[POPULATION_SIZE];
} population;

#define PIXEL_VAL(image, index, opacity, inverse_opacity, color) \
    ((unsigned char)((((int)image[index]*(inverse_opacity)) + ((int)(color)*(int)(opacity))) /  255))

#define PIXEL_CALC(image, index, opacity, inverse_opacity, col0, col1, col2) \
    image[index] = PIXEL_VAL(image, index, opacity, inverse_opacity, col0); \
    image[(index)+1] = PIXEL_VAL(image, (index)+1, opacity, inverse_opacity, col1); \
    image[(index)+2] = PIXEL_VAL(image, (index)+2, opacity, inverse_opacity, col2);

#define MUTATION(probability) (rand() % 10000 / 10000.0f < probability)

void create_segments_map(unsigned char img[],
                         unsigned int  image_width, unsigned int image_height,
                         unsigned char segment_map[]);
int gallery_create(char * directory, gallery * image_gallery);
void gallery_free(gallery * image_gallery);
void create_population(population * p, gallery * image_gallery);
void generation_cycle(population * p, gallery * image_gallery,
                      float crossover_probability,
                      float mutation_probability,
                      unsigned int winner_index);
void get_segment_centre(unsigned char img[],
                        unsigned int  image_width, unsigned int image_height,
                        unsigned char segment_index,
                        unsigned char segment_map[],
                        unsigned int * cx, unsigned int * cy);
void sample_draw(sample * s, gallery * image_gallery,
                 unsigned char output_image[],
                 unsigned int output_image_width,
                 unsigned int output_image_height);
void draw_population_sample(population * p, gallery * image_gallery,
                            int index,
                            char * filename,
                            unsigned int output_image_width,
                            unsigned int output_image_height);
void transform_point(int centre_x, int centre_y,
                     int * x, int * y,
                     int scale, int rotation,
                     int mirror, int flip);
unsigned char * read_png_file(char * filename,
                              unsigned int * width,
                              unsigned int * height,
                              unsigned int * bitsperpixel);
int write_png_file(char* filename,
                   unsigned int width, unsigned int height,
                   unsigned int bitsperpixel,
                   unsigned char *buffer);
void save_population(population * p, char * filename);
void load_population(population * p, char * filename);

#endif
