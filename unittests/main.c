/*
  Unit tests for hypercollage
  Copyright (C) 2017 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../src/hypercollage.h"

#define mu_assert(message, test) do { if (!(test)) return message; } while (0)
#define mu_run_test(test) do { char *message = test(); tests_run++; \
        if (message) return message; } while (0)
int tests_run;

static char * test_rotation() {
    int centre_x=0;
    int centre_y=0;
    int x=0;
    int y=100;
    int scale=100;
    int rotation=90;
    int mirror=0;
    int flip=0;

    transform_point(centre_x, centre_y,
                    &x, &y, scale, rotation,
                    mirror, flip);

    mu_assert("test_rotation: x != 0", x >= 0);
    mu_assert("test_rotation: y != 99", y == 99);

    x = 0;
    y = 100;
    rotation = 45;
    transform_point(centre_x, centre_y,
                    &x, &y, scale, rotation,
                    mirror, flip);
    mu_assert("test_rotation: x != 70", x == 70);
    mu_assert("test_rotation: y != 70", y == 70);

    x = 0;
    y = 100;
    rotation = -45;
    transform_point(centre_x, centre_y,
                    &x, &y, scale, rotation,
                    mirror, flip);
    mu_assert("test_rotation: x != 70", x == 70);
    mu_assert("test_rotation: y != -70", y == -70);

    centre_x = 10;
    centre_y = 20;
    x = 0;
    y = 100;
    rotation = -45;
    transform_point(centre_x, centre_y,
                    &x, &y, scale, rotation,
                    mirror, flip);
    mu_assert("test_rotation: x != 59", x == 59);
    mu_assert("test_rotation: y != -29", y == -29);

    return 0;
}

static char * run_all_tests() {
    mu_run_test(test_rotation);
    return 0;
}

int main(int argc, char* argv[])
{
    char * retval = run_all_tests();
    if (retval != 0) {
        printf("%s\n", retval);
        return 1;
    }
    printf("All tests passed\n");
    return 0;
}
