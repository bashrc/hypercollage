APP=hypercollage
VERSION=1.10
ARCH_TYPE=`uname -m`

all:
	gcc -Wall -std=c99 -pedantic -O3 -o ${APP} -Isrc src/*.c -lm -fopenmp

debug:
	gcc -Wall -std=c99 -pedantic -g -o ${APP} -Isrc src/*.c -lm -fopenmp

clean:
	rm -f src/*.plist unittests/*.plist ${APP} unittests/tests *.png

install:
	mkdir -p $(DESTDIR)/usr/bin
	mkdir -m 755 -p $(DESTDIR)/usr/share/man/man1
	install -m 755 --strip $(APP) $(DESTDIR)/usr/bin
	install -m 644 man/$(APP).1.gz $(DESTDIR)/usr/share/man/man1

uninstall:
	rm -f /usr/share/man/man1/${APP}.1.gz
	rm -f /usr/bin/${APP}
