# Hypercollage: Evolutionary art builder

<img src="https://code.freedombone.net/bashrc/hypercollage/raw/master/examples/saucers.jpg?raw=true" width=640/>

Hypercollage enables you to make collages from a directory containing 24bit RGB images. Images must be in PNG format. It loads the images, segments them and then recombines the segments to produce 9 example images. You can select one of the examples and continue evolving it, or reset for another random set of combinations.

# Installation

To build from source:

``` bash
make
sudo make install
```

# Unit tests

You can run the unit tests to check that the system is working as expected:

``` bash
cd unittests
make
./tests
```

# Usage

If you have a directory containing some PNG images, called *myimages*:

``` bash
hypercollage -i myimages
```

Make sure that you are running the command from a different directory in order to avoid confusion.

## The Next Generation

If you want to pick an image to produce the next generation:

``` bash
hypercollage -i myimages --winner [number]
```

Where the number is in the range 0 to 8.

## Starting Over

If you want to reset and get another set of random images without reference to any previous evolution:

``` bash
hypercollage -i myimages -r
```

## Image Resolution

You can also change the resolution. It will take longer to produce higher resolution images.

``` bash
hypercollage -i myimages -s 2048
```
